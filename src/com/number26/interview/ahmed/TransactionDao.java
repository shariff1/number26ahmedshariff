package com.number26.interview.ahmed;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class TransactionDao {
	public List<Transaction> getAllUsers() {
		List<Transaction> userList = null;
		try {
			File file = new File("Transaction.dat");
			if (!file.exists()) {
				Transaction user = new Transaction(1000, "Shopping", 10);
				userList = new ArrayList<Transaction>();
				userList.add(user);
				saveUserList(userList);
			} else {
				FileInputStream fis = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(fis);
				userList = (List<Transaction>) ois.readObject();
				ois.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return userList;
	}

	public Transaction getUser(Long transaction_id) {
		List<Transaction> users = getAllUsers();

		for (Transaction user : users) {
			if (user.getTransaction_id() == transaction_id) {
				return user;
			}
		}
		return null;
	}

	public int addUser(Transaction pUser) {
		List<Transaction> userList = getAllUsers();
		boolean userExists = false;
		for (Transaction user : userList) {
			if (user.getTransaction_id() == pUser.getTransaction_id()) {
				userExists = true;
				break;
			}
		}
		if (!userExists) {
			userList.add(pUser);
			saveUserList(userList);
			return 1;
		}
		return 0;
	}

	public int updateUser(Transaction pUser) {
		List<Transaction> userList = getAllUsers();

		for (Transaction user : userList) {
			if (user.getTransaction_id() == pUser.getTransaction_id()) {
				int index = userList.indexOf(user);
				userList.set(index, pUser);
				saveUserList(userList);
				return 1;
			}
		}
		return 0;
	}

	public int deleteUser(Long transaction_id) {
		List<Transaction> userList = getAllUsers();

		for (Transaction user : userList) {
			if (user.getTransaction_id() == transaction_id) {
				int index = userList.indexOf(user);
				userList.remove(index);
				saveUserList(userList);
				return 1;
			}
		}
		return 0;
	}

	private void saveUserList(List<Transaction> userList) {
		try {
			File file = new File("Transaction.dat");
			FileOutputStream fos;

			fos = new FileOutputStream(file);

			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(userList);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}