package com.number26.interview.ahmed;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "transaction")
public class Transaction implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long transaction_id;
	private double amount;
	private String type;
	private Long parent_id;

	public Transaction() {
	}

	public Transaction(double amount, String type, long parent_id) {
		this.amount = amount;
		this.type = type;
		this.parent_id = parent_id;
	}

	public Long getTransaction_id() {
		return transaction_id;
	}

	@XmlElement
	public void setTransaction_id(Long transaction_id) {
		this.transaction_id = transaction_id;
	}

	public double getAmount() {
		return amount;
	}

	@XmlElement
	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	@XmlElement
	public void setType(String type) {
		this.type = type;
	}

	public long getParent_id() {
		return parent_id;
	}

	@XmlElement
	public void setParent_id(Long parent_id) {
		this.parent_id = parent_id;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null) {
			return false;
		} else if (!(object instanceof Transaction)) {
			return false;
		} else {
			Transaction user = (Transaction) object;
			if (amount == user.getAmount() && type.equals(user.getType())
					&& parent_id.equals(user.getParent_id())
					&& transaction_id.equals(user.getTransaction_id())) {
				return true;
			}
		}
		return false;
	}
}