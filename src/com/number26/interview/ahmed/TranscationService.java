package com.number26.interview.ahmed;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("/TranscationService")
public class TranscationService {
	
   TransactionDao userDao = new TransactionDao();
   private static final String SUCCESS_RESULT="<result>success</result>";
   private static final String FAILURE_RESULT="<result>failure</result>";


   @GET
   @Path("/transcations")
   @Produces(MediaType.APPLICATION_XML)
   public List<Transaction> getUsers(){
      return userDao.getAllUsers();
   }

   @GET
   @Path("/transcations/{transaction_id}")
   @Produces(MediaType.APPLICATION_XML)
   public Transaction getTransaction(@PathParam("transaction_id") Long transaction_id){
      return userDao.getUser(transaction_id);
   }

   @PUT
   @Path("/transactions")
   @Produces(MediaType.APPLICATION_XML)
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   public String createUser(@FormParam("transaction_id") Long transaction_id,
      @FormParam("amount") double amount,
      @FormParam("type") String type,
      @FormParam("parent_id") Long parent_id,
      @Context HttpServletResponse servletResponse) throws IOException{
      Transaction user = new Transaction( amount, type,parent_id);
      int result = userDao.addUser(user);
      if(result == 1){
         return SUCCESS_RESULT;
      }
      return FAILURE_RESULT;
   }

   @POST
   @Path("/transactions")
   @Produces(MediaType.APPLICATION_XML)
   @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
   public String updateUser(@FormParam("transaction_id") Long transaction_id,
		      @FormParam("amount") double amount,
		      @FormParam("type") String type,
		      @FormParam("parent_id") Long parent_id,
      @Context HttpServletResponse servletResponse) throws IOException{
      Transaction user = new Transaction(amount, type, parent_id);
      int result = userDao.updateUser(user);
      if(result == 1){
         return SUCCESS_RESULT;
      }
      return FAILURE_RESULT;
   }

   @DELETE
   @Path("/transactions/{transaction_id}")
   @Produces(MediaType.APPLICATION_XML)
   public String deleteUser(@PathParam("transaction_id") Long transaction_id){
      int result = userDao.deleteUser(transaction_id);
      if(result == 1){
         return SUCCESS_RESULT;
      }
      return FAILURE_RESULT;
   }

   @OPTIONS
   @Path("/transactions")
   @Produces(MediaType.APPLICATION_XML)
   public String getSupportedOperations(){
      return "<operations>GET, PUT, POST, DELETE</operations>";
   }
}