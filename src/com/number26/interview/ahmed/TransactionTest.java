package com.number26.interview.ahmed;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

public class TransactionTest  {

   private Client client;
   private String REST_SERVICE_URL = "http://localhost:8080/RestNumber26Ahmed/transactionservice/transaction/";
   private static final String SUCCESS_RESULT="<result>success</result>";
   private static final String PASS = "pass";
   private static final String FAIL = "fail";

   private void init(){
      this.client = ClientBuilder.newClient();
   }

   public static void main(String[] args){
      TransactionTest tester = new TransactionTest();
      //initialize the tester
      tester.init();
      //test get all transaction Web Service Method
      tester.testGetAlltransactions();
      //test get transaction Web Service Method 
      tester.testGettransaction();
      //test update transaction Web Service Method
      tester.testUpdatetransaction();
      //test add transaction Web Service Method
      tester.testAddtransaction();
      //test delete transaction Web Service Method
      tester.testDeletetransaction();
   }
   //Test: Get list of all transactions
   //Test: Check if list is not empty
   private void testGetAlltransactions(){
      GenericType<List<Transaction>> list = new GenericType<List<Transaction>>() {};
      List<Transaction> transactions = client
         .target(REST_SERVICE_URL)
         .request(MediaType.APPLICATION_XML)
         .get(list);
      String result = PASS;
      if(transactions.isEmpty()){
         result = FAIL;
      }
      System.out.println("Test case name: testGetAlltransactions, Result: " + result );
   }
   //Test: Get transaction of id 1
   //Test: Check if transaction is same as sample transaction
   private void testGettransaction(){
	   Transaction sampletransaction = new Transaction();
      sampletransaction.setTransaction_id(1L);

      Transaction transaction = client
         .target(REST_SERVICE_URL)
         .path("/{transaction_id}")
         .resolveTemplate("transaction_id", 1)
         .request(MediaType.APPLICATION_XML)
         .get(Transaction.class);
      String result = FAIL;
      if(sampletransaction != null && sampletransaction.getTransaction_id() == transaction.getTransaction_id()){
         result = PASS;
      }
      System.out.println("Test case name: testGettransaction, Result: " + result );
   }
   //Test: Update transaction of id 1
   //Test: Check if result is success XML.
   private void testUpdatetransaction(){
      Form form = new Form();
      form.param("amount","1000" );
      form.param("type", "Shopping");
      form.param("parent_id", "10");

      String callResult = client
         .target(REST_SERVICE_URL)
         .request(MediaType.APPLICATION_XML)
         .post(Entity.entity(form,
            MediaType.APPLICATION_FORM_URLENCODED_TYPE),
            String.class);
      String result = PASS;
      if(!SUCCESS_RESULT.equals(callResult)){
         result = FAIL;
      }

      System.out.println("Test case name: testUpdatetransaction, Result: " + result );
   }
   //Test: Add transaction of id 2
   //Test: Check if result is success .
   private void testAddtransaction(){
      Form form = new Form();
      form.param("amount","2000" );
      form.param("type", "bag");
      form.param("parent_id", "100");

      String callResult = client
         .target(REST_SERVICE_URL)
         .request(MediaType.APPLICATION_XML)
         .put(Entity.entity(form,
            MediaType.APPLICATION_FORM_URLENCODED_TYPE),
            String.class);
   
      String result = PASS;
      if(!SUCCESS_RESULT.equals(callResult)){
         result = FAIL;
      }

      System.out.println("Test case name: testAddtransaction, Result: " + result );
   }
   //Test: Delete transaction of id 2
   //Test: Check if result is success XML.
   private void testDeletetransaction(){
      String callResult = client
         .target(REST_SERVICE_URL)
         .path("/{transaction_id}")
         .resolveTemplate("transaction_id", 2)
         .request(MediaType.APPLICATION_XML)
         .delete(String.class);

      String result = PASS;
      if(!SUCCESS_RESULT.equals(callResult)){
         result = FAIL;
      }

      System.out.println("Test case name: testDeletetransaction, Result: " + result );
   }
}